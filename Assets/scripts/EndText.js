﻿#pragma strict
var tex : Texture2D;

var FadeOutTimer : float = 0;
var alphaFadeValue : float = 0;
function Start () {

}

function Update () {

FadeOutTimer = FadeOutTimer + (1 * Time.deltaTime);

}

function OnGUI()
{
GUI.DrawTexture (Rect (0,0, Screen.width, Screen.height), tex);
GUI.Label(Rect.MinMaxRect(Screen.width / 12,Screen.height / 12,Screen.width - (Screen.width / 12),Screen.height),"<color=grey><size=20>The father didn't know why he'd come back after all these years, starving and tired.\nHe just wanted to see the cellar one last time.\nWith one last flicker, the flashlight died and he was left in the pitch dark.\nHe sat down on the cold floor and closed his eyes.\nHe could still hear her, even now.\nIt was peaceful here in the cellar...\nIt was free here in the cellar...</size></color>");


    
if (FadeOutTimer > 18)
{            
alphaFadeValue += Mathf.Clamp01(Time.deltaTime / 10);
}
   
GUI.color = new Color(0, 0, 0, alphaFadeValue);
GUI.DrawTexture( new Rect(0, 0, Screen.width, Screen.height ), tex );    
    
if (alphaFadeValue > 1) 
{
Application.Quit();
}      
                
}