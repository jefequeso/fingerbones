﻿#pragma strict
//public class DisplayNote
//{


public static var NoteToDisplay : int = 0;
public static var LastNote : int = 0;


var tex : Texture2D;
var tex2 : Texture2D;
var tex3 : Texture2D;
var tex4 : Texture2D;

var blackTexture : Texture2D;

public static var alphaFadeValue : float;

public static var FadeOut : int = 0;


function Start () {

}

function Update () {

//if (NoteToDisplay != 0) {MouseLook.DoMouse = false;}


if (NoteToDisplay == 15) 
    {
    
    if (Input.GetButtonDown("Use")) {LastNote = LastNote + 1;}  
    }  

}

function OnGUI(){
 //GUI.skin.font = font;
 //GUI.color = color;

GUI.DrawTexture (Rect (0,0, Screen.width, Screen.height), tex2);
  
    
 GUI.depth = 0;
 	if (NoteToDisplay > 0)
 	{
 	GUI.DrawTexture (Rect (0,0, Screen.width, Screen.height), tex);
    if (NoteToDisplay == 1) {GUI.Label(Rect.MinMaxRect(Screen.width / 12,Screen.height / 12,Screen.width - (Screen.width / 12),Screen.height),"<color=grey><size=20>It's my weekend to get Katie, but I'm really not in the mood.  I distracted her with the TV and came here to read.  I couldn't interest myself in reading, so I tried to write.  I couldn't write anything more than the usual handful of disconnected thoughts and theories.  Medication didn't help and divorce certainly didn't help.  Because I am not depressed--I am enlightened.  This is the result of two decades of scientific education and philosophical pondering.  This is the price of knowledge.</size></color>");}
    if (NoteToDisplay == 2) {GUI.Label(Rect.MinMaxRect(Screen.width / 12,Screen.height / 12,Screen.width - (Screen.width / 12),Screen.height),"<color=grey><size=20>[It appears to be a page from a manuscript] Pop morality is too reliant on emotions, and not reliant enough on rational thought. If morality must be determined (and I maintain that it is a farce), it should not be determined by feeling.  Feelings are nebulous, subjective, and changeable.  Rather, it should be determined by thinking.  The actions of a natural creature are natural and thus moral, unless a concrete scientific reason can be given to prove them as immoral.</size></color>");}
    if (NoteToDisplay == 3) {GUI.Label(Rect.MinMaxRect(Screen.width / 12,Screen.height / 12,Screen.width - (Screen.width / 12),Screen.height),"<color=grey><size=20>I don't know why I'd use my daughter's name as a door lock code in my survival bunker, but whatever keeps her happy, I suppose.  She's like her mother.  A delicate, fragile version of her mother.  Damn her.</size></color>");}
    if (NoteToDisplay == 4) {GUI.Label(Rect.MinMaxRect(Screen.width / 12,Screen.height / 12,Screen.width - (Screen.width / 12),Screen.height),"<color=grey><size=20>The tool room has been her name for years now, and I can't bring myself to change it.</size></color>");}
    if (NoteToDisplay == 5) {GUI.Label(Rect.MinMaxRect(Screen.width / 12,Screen.height / 12,Screen.width - (Screen.width / 12),Screen.height),"<color=grey><size=20>I boxed up everything that isn't practical and useful.  I burned all the books that weren't for scientific edification.  I'd like to store the boxes in the cellar, but I can't get them down the ladder by myself.  I just piled them up near the hatch.  The blood would probably ruin them anyway.</size></color>");}
    if (NoteToDisplay == 6) {GUI.Label(Rect.MinMaxRect(Screen.width / 12,Screen.height / 12,Screen.width - (Screen.width / 12),Screen.height),"<color=grey><size=20>It's finally happened.  The whole world is a chaos of thunder and screams, and when the ashes settle we will all be wild animals again.  I am ecstatic beyond words.  I haven't been able to reach Lynn for several days, and she's probably dead for all I know or care.  But Katie was here when it started, and I have no idea what to do with her.  Most of the town is dead.  Katie and I only survived because we ran to the bunker.  Perhaps this is what I've needed.  Freedom from all the irrational rules and inane social values.  Just pure, beautiful survival.</size></color>");}
    if (NoteToDisplay == 7) {GUI.Label(Rect.MinMaxRect(Screen.width / 12,Screen.height / 12,Screen.width - (Screen.width / 12),Screen.height),"<color=grey><size=20>[It appears to be torn from a manuscript] Altruism is just a less obvious expression of self interest.  It's a consequence of the same mathematical cost-benefit analysis that guides the rest of my decisions.  It is illogical to make it a moral barrier to self interest.</size></color>");}
    if (NoteToDisplay == 8) {GUI.Label(Rect.MinMaxRect(Screen.width / 12,Screen.height / 12,Screen.width - (Screen.width / 12),Screen.height),"<color=grey><size=20>She remembered Father's Day, and drew me a picture.  I don't understand her.  She's just like her mother.  She wants to waste time drawing pictures, and we're barely able to find enough food to survive.  I turned her crayons into candles.  We need more candles in case the generator goes out again, and it helps break up the monotony.  I yearn for the excitement of those first weeks.</size></color>");}
    if (NoteToDisplay == 9) {GUI.Label(Rect.MinMaxRect(Screen.width / 12,Screen.height / 12,Screen.width - (Screen.width / 12),Screen.height),"<color=grey><size=20>I changed the storage room code to something I can easily remember.  nny [the rest of the word is missing]</size></color>");}
    if (NoteToDisplay == 10) {GUI.Label(Rect.MinMaxRect(Screen.width / 12,Screen.height / 12,Screen.width - (Screen.width / 12),Screen.height),"<color=grey><size=20>There are things I've wanted to do for years.  Desires I've kept locked in the back of my consciouness.  It's been nearly a year since I've seen a woman, and now I realize there's no practical reason to keep those desires locked up anymore.</size></color>");}
    if (NoteToDisplay == 11) {GUI.Label(Rect.MinMaxRect(Screen.width / 12,Screen.height / 12,Screen.width - (Screen.width / 12),Screen.height),"<color=grey><size=20>I am a bag of DNA, and I exist to make more of myself.  I married and produced a daughter because my genes demand reproduction.  I wrote books and created, because creativity served a survival advantage to my ancient ape ancestors.  I built this place, took refuge in it, and spent hours thinking up silly little artificial laws to live by, because my forefathers built cities and societies, to allow time and peace to reproduce and to protect their genes.  Dreams, loves, opinions, desires, beauty, innocence... figments of our collective primordial imagination.  Fleeting electrical signals that fire across our synapses for a pointless moment in time. They used to serve a purpose, and now they are needless confusion.  And here I am, the last man on earth for all I know, ready to be freed from them.</size></color>");}
    if (NoteToDisplay == 12) {GUI.Label(Rect.MinMaxRect(Screen.width / 12,Screen.height / 12,Screen.width - (Screen.width / 12),Screen.height),"<color=grey><size=20>When I was 15 years old I lost my virginity.  I shook uncontrollably the entire time.  I felt that same primal excitement yesterday night in the cellar.</size></color>");}
    if (NoteToDisplay == 13) {GUI.Label(Rect.MinMaxRect(Screen.width / 12,Screen.height / 12,Screen.width - (Screen.width / 12),Screen.height),"<color=grey><size=20>The cellar is freedom.</size></color>");}
    if (NoteToDisplay == 14) {GUI.Label(Rect.MinMaxRect(Screen.width / 12,Screen.height / 12,Screen.width - (Screen.width / 12),Screen.height),"<color=grey><size=20>There's only enough food for one, so she's going to die anyway.  And I'm free now.  Freedom.  It took the end of all humanity for me to find freedom.</size></color>");}
    
    if (NoteToDisplay == 15) 
    {
    
    //if (Input.GetButtonDown("Use")) {LastNote = LastNote + 1;}
    if (LastNote == 0) {GUI.Label(Rect.MinMaxRect(Screen.width / 12,Screen.height / 12,Screen.width - (Screen.width / 12),Screen.height),"<color=grey><size=20>[It seems to have been torn from a diary]</size></color>");}
    if (LastNote == 1) {GUI.Label(Rect.MinMaxRect(Screen.width / 12,Screen.height / 12,Screen.width - (Screen.width / 12),Screen.height),"<color=grey><size=20>She finally died last night.  I don't know whether from starvation, dehydration, or blood loss.  I threw her body into the woods.  Her fingers--my anti-religious artifacts... my testements to moral and intellectual freedom--are locked in the safe upstairs.</size></color>");}
    if (LastNote == 2) {GUI.Label(Rect.MinMaxRect(Screen.width / 12,Screen.height / 12,Screen.width - (Screen.width / 12),Screen.height),"<color=grey><size=20>I am content in the knowledge that I haven't done anything wrong.</size></color>");}
 
    if (LastNote == 3) 
    {
    LastNote = 0;
    NoteToDisplay = 0;
    FadeOut = 1;
    }
    
    
    //print(LastNote);
    
    
    
    }
    
    
    }
    if (Application.loadedLevel > 0) {GUI.DrawTexture (Rect (0,0, Screen.width, Screen.height), tex3);}
    //if (Application.loadedLevel == 2) {GUI.DrawTexture (Rect (0,0, Screen.width, Screen.height), tex4);}
    GUI.depth = 1;

    
    
    if (DisplayEscMenu.ShowEscMenu == false && FadeOut == 0) {alphaFadeValue -= Mathf.Clamp01(Time.deltaTime / 10);}
    if (alphaFadeValue < 0) {alphaFadeValue = 0;}
    if (FadeOut == 1) 
    {
    alphaFadeValue += Mathf.Clamp01(Time.deltaTime / 10);
    if (alphaFadeValue > 1) 
    {
    Destroy(GameObject.Find("ZaPlayer(Clone)"));
    Application.LoadLevel (3);
    }
    }
    
    GUI.color = new Color(0, 0, 0, alphaFadeValue);
    GUI.DrawTexture( new Rect(0, 0, Screen.width, Screen.height ), blackTexture );
    
        
    }
   
     
//}