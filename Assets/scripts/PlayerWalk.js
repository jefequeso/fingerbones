﻿#pragma strict

var walkAccel : float;
var cameraObject : GameObject;
var maxWalkSpeed : float;
var horizMovement : Vector2;

var walkDeacceleration : float;

@HideInInspector

var walkDeaccelerationVolx : float;

@HideInInspector

var walkDeaccelerationVolz : float;



function Start () {

}

function Update () {

horizMovement = Vector2(rigidbody.velocity.x,rigidbody.velocity.z);

if (horizMovement.magnitude > maxWalkSpeed) {
horizMovement = horizMovement.normalized;
horizMovement *= maxWalkSpeed;


 } 

rigidbody.velocity.x = horizMovement.x;
rigidbody.velocity.z = horizMovement.y;

 rigidbody.velocity.x = Mathf.SmoothDamp(rigidbody.velocity.x, 0, walkDeaccelerationVolx, walkDeacceleration);
 rigidbody.velocity.z = Mathf.SmoothDamp(rigidbody.velocity.z, 0, walkDeaccelerationVolz, walkDeacceleration);

    

transform.rotation = Quaternion.Euler(0,cameraObject.GetComponent(MouseLookScript).currentYRotation,0);
rigidbody.AddRelativeForce(0,0,Input.GetAxis("Forward") * walkAccel);


 
}

