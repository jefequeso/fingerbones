﻿#pragma strict
var StorageDoor : GameObject;
var ToolDoor : GameObject;
var readtex : Texture2D;
var usetex : Texture2D;
var locktex : Texture2D;
var exittex : Texture2D;
public static var UnlockedToLower : boolean = false;
public static var UnlockedToTools : boolean = false;
public static var UnlockedToStorage : boolean = false;
public static var UnlockedToCellar : boolean = false;

public static var DestroySafe : boolean = false;
public static var GeneratorOn : boolean = false;

public static var AlreadyPlayedTools : boolean = false;
public static var AlreadyPlayedStorage : boolean = false;

public static var PlayUnlocked : int = 0;




var ClosedDoorSound : AudioClip;
var ReadNoteSound : AudioClip;
var OpenDoorSound : AudioClip;
var FlashlightOn : AudioClip;
var UnlockDoorSound : AudioClip;
var HatchOpen : AudioClip;
var GenStart : AudioClip;
var ButtonPress : AudioClip;

function Start () {

}

function Update () {

if (PlayUnlocked == 1) 
{
audio.PlayOneShot(UnlockDoorSound,AudioListener.volume);
PlayUnlocked = 0;
//print("hello!");
}


// clear note if move
if (Input.GetButtonDown("Vertical") || Input.GetButtonDown("Horizontal")) 
{
if (DisplayNote.NoteToDisplay != 15)
{
DisplayNote.NoteToDisplay = 0;
}
}











var fwd = transform.TransformDirection (Vector3.forward);
var hit : RaycastHit;

if (Input.GetButtonDown("Use")) 
{



if (DisplayNote.NoteToDisplay == 0) 
{




if (Physics.Raycast (transform.position, fwd, hit, 2)) 
{
//if (hit.collider.gameObject.name == "Button"){

//print ("There is something in front of the object!");
//target.animation.Play("DoorAnimation");
//}




// notes
if (hit.collider.gameObject.name == "Note1") 
{
DisplayNote.NoteToDisplay = 1;
audio.PlayOneShot(ReadNoteSound,AudioListener.volume);
}

if (hit.collider.gameObject.name == "Note2") 
{
DisplayNote.NoteToDisplay = 2;
audio.PlayOneShot(ReadNoteSound,AudioListener.volume);
}

if (hit.collider.gameObject.name == "Note3") 
{
DisplayNote.NoteToDisplay = 3;
audio.PlayOneShot(ReadNoteSound,AudioListener.volume);
}

if (hit.collider.gameObject.name == "Note4") 
{
DisplayNote.NoteToDisplay = 4;
audio.PlayOneShot(ReadNoteSound,AudioListener.volume);
}

if (hit.collider.gameObject.name == "Note5") 
{
DisplayNote.NoteToDisplay = 5;
audio.PlayOneShot(ReadNoteSound,AudioListener.volume);
}

if (hit.collider.gameObject.name == "Note6") 
{
DisplayNote.NoteToDisplay = 6;
audio.PlayOneShot(ReadNoteSound,AudioListener.volume);
}

if (hit.collider.gameObject.name == "Note7") 
{
DisplayNote.NoteToDisplay = 7;
audio.PlayOneShot(ReadNoteSound,AudioListener.volume);
}

if (hit.collider.gameObject.name == "Note8") 
{
DisplayNote.NoteToDisplay = 8;
audio.PlayOneShot(ReadNoteSound,AudioListener.volume);
}

if (hit.collider.gameObject.name == "Note9") 
{
DisplayNote.NoteToDisplay = 9;
audio.PlayOneShot(ReadNoteSound,AudioListener.volume);
}

if (hit.collider.gameObject.name == "Note10") 
{
DisplayNote.NoteToDisplay = 10;
audio.PlayOneShot(ReadNoteSound,AudioListener.volume);
}

if (hit.collider.gameObject.name == "Note11") 
{
DisplayNote.NoteToDisplay = 11;
audio.PlayOneShot(ReadNoteSound,AudioListener.volume);
}

if (hit.collider.gameObject.name == "Note12") 
{
DisplayNote.NoteToDisplay = 12;
audio.PlayOneShot(ReadNoteSound,AudioListener.volume);
}

if (hit.collider.gameObject.name == "Note13") 
{
DisplayNote.NoteToDisplay = 13;
audio.PlayOneShot(ReadNoteSound,AudioListener.volume);
}

if (hit.collider.gameObject.name == "Note14") 
{
DisplayNote.NoteToDisplay = 14;
audio.PlayOneShot(ReadNoteSound,AudioListener.volume);
}

if (hit.collider.gameObject.name == "Note15") 
{
DisplayNote.NoteToDisplay = 15;
audio.PlayOneShot(ReadNoteSound,AudioListener.volume);
}

if (hit.collider.gameObject.name == "DoorToLower") 
{
if  (UnlockedToLower == true)
{
audio.PlayOneShot(OpenDoorSound,AudioListener.volume);
DisplayNote.alphaFadeValue = 1;
PlayerSwitchLevels.GoToLevel = 2;
}
else
{
audio.PlayOneShot(ClosedDoorSound,AudioListener.volume);
}
}

if (hit.collider.gameObject.name == "DoorToL1") 
{
audio.PlayOneShot(OpenDoorSound,AudioListener.volume);
DisplayNote.alphaFadeValue = 1;
PlayerSwitchLevels.GoToLevel = 1;
}

if (hit.collider.gameObject.name == "HatchToL3") 
{
if (UnlockedToCellar == true) 
{
audio.PlayOneShot(HatchOpen,AudioListener.volume);
DisplayNote.alphaFadeValue = 1;
PlayerSwitchLevels.GoToLevel = 3;
}
else
{
audio.PlayOneShot(ClosedDoorSound,AudioListener.volume);
}
}

if (hit.collider.gameObject.name == "HatchToL2") 
{
audio.PlayOneShot(HatchOpen,AudioListener.volume);
DisplayNote.alphaFadeValue = 1;
PlayerSwitchLevels.GoToLevel = 4;
}

if (hit.collider.gameObject.name == "keyboard" && GeneratorOn == true) 
{
EnterText.DisplayInput = true;
}

if (hit.collider.gameObject.name == "SafeButton" && DestroySafe == false) 
{
(GameObject.Find("SafeButton")).animation.Play("ButtonPressAnimate");
audio.PlayOneShot(ButtonPress,AudioListener.volume);
DestroySafe = true;
}

if (hit.collider.gameObject.name == "GeneratorButton" && GeneratorOn == false) 
{
(GameObject.Find("GeneratorButton")).animation.Play("ButtonPressAnimate2");
audio.PlayOneShot(GenStart,AudioListener.volume);
GeneratorOn = true;
}

if (hit.collider.gameObject.name == "DoorToTools" && AlreadyPlayedTools == false) 
{
if (UnlockedToTools == true)
{
audio.PlayOneShot(OpenDoorSound,AudioListener.volume);
StorageDoor = GameObject.Find("DoorToToolsPivot");
StorageDoor.animation.Play("DoorAnimation");
AlreadyPlayedTools = true;
}
else
{
audio.PlayOneShot(ClosedDoorSound,AudioListener.volume);
}
}

if (hit.collider.gameObject.name == "DoorToStorage" && AlreadyPlayedStorage == false) 
{
if (UnlockedToStorage == true)
{
audio.PlayOneShot(OpenDoorSound,AudioListener.volume);
StorageDoor = GameObject.Find("DoorToStoragePivot");
StorageDoor.animation.Play("DoorAnimation2");
AlreadyPlayedStorage = true;
}
else
{
audio.PlayOneShot(ClosedDoorSound,AudioListener.volume);
}
}

if (hit.collider.gameObject.name == "ExitDoor") 
{
Application.Quit();
}


// Flashlight
if (hit.collider.gameObject.name == "flashlightobject")
{
Destroy(GameObject.Find("flashlightobject"));
audio.PlayOneShot(FlashlightOn,AudioListener.volume);
FlashlightOnOffScript.OnOff = 1;
}

}


}
else
{
if (DisplayNote.NoteToDisplay != 15) {DisplayNote.NoteToDisplay = 0;}
}







}


















}

function OnGUI()
{
var fwd = transform.TransformDirection (Vector3.forward);
var hit : RaycastHit;


// display cursor
if (DisplayNote.NoteToDisplay == 0) {



if (Physics.Raycast (transform.position, fwd, hit, 2)) 
{
if (hit.collider.gameObject.name != "ExitDoor") 
{
// draw default cursor
GUI.DrawTexture(Rect.MinMaxRect((Screen.width / 2) - 16,(Screen.height / 2) - 16, (Screen.width / 2) + 16,(Screen.height / 2) + 16), usetex);
}
else
{
GUI.DrawTexture(Rect.MinMaxRect((Screen.width / 2) - 32,(Screen.height / 2) - 32, (Screen.width / 2) + 32,(Screen.height / 2) + 32), exittex);
}

// notes
if (hit.collider.gameObject.name == "Note1") 
{
GUI.DrawTexture(Rect.MinMaxRect((Screen.width / 2) - 32,(Screen.height / 2) - 32, (Screen.width / 2) + 32,(Screen.height / 2) + 32), readtex);
}

if (hit.collider.gameObject.name == "Note2") 
{
GUI.DrawTexture(Rect.MinMaxRect((Screen.width / 2) - 32,(Screen.height / 2) - 32, (Screen.width / 2) + 32,(Screen.height / 2) + 32), readtex);
}

if (hit.collider.gameObject.name == "Note3") 
{
GUI.DrawTexture(Rect.MinMaxRect((Screen.width / 2) - 32,(Screen.height / 2) - 32, (Screen.width / 2) + 32,(Screen.height / 2) + 32), readtex);
}

if (hit.collider.gameObject.name == "Note4") 
{
GUI.DrawTexture(Rect.MinMaxRect((Screen.width / 2) - 32,(Screen.height / 2) - 32, (Screen.width / 2) + 32,(Screen.height / 2) + 32), readtex);
}

if (hit.collider.gameObject.name == "Note5") 
{
GUI.DrawTexture(Rect.MinMaxRect((Screen.width / 2) - 32,(Screen.height / 2) - 32, (Screen.width / 2) + 32,(Screen.height / 2) + 32), readtex);
}

if (hit.collider.gameObject.name == "Note6") 
{
GUI.DrawTexture(Rect.MinMaxRect((Screen.width / 2) - 32,(Screen.height / 2) - 32, (Screen.width / 2) + 32,(Screen.height / 2) + 32), readtex);
}

if (hit.collider.gameObject.name == "Note7") 
{
GUI.DrawTexture(Rect.MinMaxRect((Screen.width / 2) - 32,(Screen.height / 2) - 32, (Screen.width / 2) + 32,(Screen.height / 2) + 32), readtex);
}

if (hit.collider.gameObject.name == "Note8") 
{
GUI.DrawTexture(Rect.MinMaxRect((Screen.width / 2) - 32,(Screen.height / 2) - 32, (Screen.width / 2) + 32,(Screen.height / 2) + 32), readtex);
}

if (hit.collider.gameObject.name == "Note9") 
{
GUI.DrawTexture(Rect.MinMaxRect((Screen.width / 2) - 32,(Screen.height / 2) - 32, (Screen.width / 2) + 32,(Screen.height / 2) + 32), readtex);
}

if (hit.collider.gameObject.name == "Note10") 
{
GUI.DrawTexture(Rect.MinMaxRect((Screen.width / 2) - 32,(Screen.height / 2) - 32, (Screen.width / 2) + 32,(Screen.height / 2) + 32), readtex);
}

if (hit.collider.gameObject.name == "Note11") 
{
GUI.DrawTexture(Rect.MinMaxRect((Screen.width / 2) - 32,(Screen.height / 2) - 32, (Screen.width / 2) + 32,(Screen.height / 2) + 32), readtex);
}

if (hit.collider.gameObject.name == "Note12") 
{
GUI.DrawTexture(Rect.MinMaxRect((Screen.width / 2) - 32,(Screen.height / 2) - 32, (Screen.width / 2) + 32,(Screen.height / 2) + 32), readtex);
}

if (hit.collider.gameObject.name == "Note13") 
{
GUI.DrawTexture(Rect.MinMaxRect((Screen.width / 2) - 32,(Screen.height / 2) - 32, (Screen.width / 2) + 32,(Screen.height / 2) + 32), readtex);
}

if (hit.collider.gameObject.name == "Note14") 
{
GUI.DrawTexture(Rect.MinMaxRect((Screen.width / 2) - 32,(Screen.height / 2) - 32, (Screen.width / 2) + 32,(Screen.height / 2) + 32), readtex);
}

if (hit.collider.gameObject.name == "Note15") 
{
GUI.DrawTexture(Rect.MinMaxRect((Screen.width / 2) - 32,(Screen.height / 2) - 32, (Screen.width / 2) + 32,(Screen.height / 2) + 32), readtex);
}

if (hit.collider.gameObject.name == "keyboard") 
{
GUI.DrawTexture(Rect.MinMaxRect((Screen.width / 2) - 32,(Screen.height / 2) - 32, (Screen.width / 2) + 32,(Screen.height / 2) + 32), readtex);
}

if (hit.collider.gameObject.name == "SafeButton" && DestroySafe == false) 
{
GUI.DrawTexture(Rect.MinMaxRect((Screen.width / 2) - 32,(Screen.height / 2) - 32, (Screen.width / 2) + 32,(Screen.height / 2) + 32), readtex);
}

if (hit.collider.gameObject.name == "GeneratorButton" && GeneratorOn == false) 
{
GUI.DrawTexture(Rect.MinMaxRect((Screen.width / 2) - 32,(Screen.height / 2) - 32, (Screen.width / 2) + 32,(Screen.height / 2) + 32), readtex);
}


// Doors
if (hit.collider.gameObject.name == "DoorToLower") 
{
GUI.DrawTexture(Rect.MinMaxRect((Screen.width / 2) - 32,(Screen.height / 2) - 32, (Screen.width / 2) + 32,(Screen.height / 2) + 32), readtex);
}

if (hit.collider.gameObject.name == "HatchToL3") 
{
GUI.DrawTexture(Rect.MinMaxRect((Screen.width / 2) - 32,(Screen.height / 2) - 32, (Screen.width / 2) + 32,(Screen.height / 2) + 32), readtex);
}

if (hit.collider.gameObject.name == "DoorToL1") 
{
GUI.DrawTexture(Rect.MinMaxRect((Screen.width / 2) - 32,(Screen.height / 2) - 32, (Screen.width / 2) + 32,(Screen.height / 2) + 32), readtex);
}

if (hit.collider.gameObject.name == "HatchToL2") 
{
GUI.DrawTexture(Rect.MinMaxRect((Screen.width / 2) - 32,(Screen.height / 2) - 32, (Screen.width / 2) + 32,(Screen.height / 2) + 32), readtex);
}

if (hit.collider.gameObject.name == "DoorToTools" && AlreadyPlayedTools == false) 
{
GUI.DrawTexture(Rect.MinMaxRect((Screen.width / 2) - 32,(Screen.height / 2) - 32, (Screen.width / 2) + 32,(Screen.height / 2) + 32), readtex);
}

if (hit.collider.gameObject.name == "DoorToStorage" && AlreadyPlayedStorage == false) 
{
GUI.DrawTexture(Rect.MinMaxRect((Screen.width / 2) - 32,(Screen.height / 2) - 32, (Screen.width / 2) + 32,(Screen.height / 2) + 32), readtex);
}





// flashlight
if (hit.collider.gameObject.name == "flashlightobject") 
{
GUI.DrawTexture(Rect.MinMaxRect((Screen.width / 2) - 32,(Screen.height / 2) - 32, (Screen.width / 2) + 32,(Screen.height / 2) + 32), usetex);
}



}





}



}